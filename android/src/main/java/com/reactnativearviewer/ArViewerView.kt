package com.reactnativearviewer

import android.animation.ObjectAnimator
import android.app.Activity
import android.app.ActivityManager
import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Handler
import android.os.Looper
import android.util.AttributeSet
import android.util.Base64
import android.util.Log
import android.view.MotionEvent
import android.view.PixelCopy
import android.view.ViewGroup
import android.view.ViewTreeObserver.OnWindowFocusChangeListener
import android.widget.FrameLayout
import com.facebook.react.bridge.Arguments
import com.facebook.react.uimanager.ThemedReactContext
import com.facebook.react.uimanager.events.RCTEventEmitter
import com.google.android.filament.utils.Float3
import com.google.ar.core.*
import com.google.ar.core.ArCoreApk.InstallStatus
import com.google.ar.core.exceptions.UnavailableException
import com.google.ar.sceneform.*
import com.google.ar.sceneform.math.Vector3
import com.google.ar.sceneform.rendering.CameraStream
import com.google.ar.sceneform.rendering.ModelRenderable
import com.google.ar.sceneform.ux.BaseArFragment.OnSessionConfigurationListener
import com.google.ar.sceneform.ux.FootprintSelectionVisualizer
import com.google.ar.sceneform.ux.TransformationSystem
import com.gorisse.thomas.sceneform.environment
import java.io.ByteArrayOutputStream
import java.util.concurrent.Executors


class ArViewerView @JvmOverloads constructor(
    context: ThemedReactContext, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr), Scene.OnPeekTouchListener, Scene.OnUpdateListener {
    /**
     * We show only one model, let's store the ref here
     */
    private val modelNodes = mutableListOf<CustomTransformableNode>()

    /**
     * Our main view that integrates with ARCore and renders a scene
     */
    private var arView: ArSceneView? = null

    /**
     * Event listener that triggers on focus
     */
    private val onFocusListener = OnWindowFocusChangeListener { onWindowFocusChanged(it) }

    /**
     * Event listener that triggers when the ARCore Session is to be configured
     */
    private val onSessionConfigurationListener: OnSessionConfigurationListener? = null

    /**
     * Main view state
     */
    private var isStarted = false

    /**
     * ARCore installation requirement state
     */
    private var installRequested = false

    /**
     * Failed session initialization state
     */
    private var sessionInitializationFailed = false

    /**
     * Depth management enabled state
     */
    private var isDepthManagementEnabled = false

    /**
     * Light estimation enabled state
     */
    private var isLightEstimationEnabled = false

    /**
     * Instant placement enabled state
     */
    private var isInstantPlacementEnabled = true

    /**
     * Detection type
     */
    private var detectionType: String = ""

    /**
     * Instructions enabled state
     */
    private var isInstructionsEnabled = true

    /**
     * Device supported state
     */
    private var isDeviceSupported = true

    /**
     * Reminder to keep track of model loading state
     */
    private var isLoading = false

    /**
     * Config of the main session initialization
     */
    private var sessionConfig: Config? = null

    /**
     * AR session initialization
     */
    private var arSession: Session? = null

    /**
     * Instructions controller initialization
     */
    private var instructionsController: InstructionsController? = null

    /**
     * Transformation system initialization
     */
    private var transformationSystem: TransformationSystem? = null

    /**
     * Gesture detector initialization
     */
//    private var gestureDetector: GestureDetector? = null

    /**
     * Reminder to keep source of model loading
     */
    private var modelSrc: String = ""

    /**
     * Set of allowed model transformations (rotate, scale, translate...)
     */
    private var allowTransform = mutableSetOf<String>()
    private lateinit var imageDatabase: AugmentedImageDatabase
    private val trackableMap = mutableMapOf<String, Trackable>()
    private val anchorMap = mutableMapOf<String, Anchor>()
    private val lastAnimators = mutableListOf<ObjectAnimator?>()

    init {
        if (checkIsSupportedDevice(context.currentActivity!!)) {
            // check AR Core installation
            if (requestInstall()) {
                returnErrorEvent("ARCore installation required")
                isDeviceSupported = false
            } else {
                // let's create sceneform view
                arView = ArSceneView(context, attrs)
                arView!!.layoutParams = LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT
                )
                this.addView(arView)

                transformationSystem = makeTransformationSystem()

                arView!!.scene.addOnPeekTouchListener(this)
                arView!!.scene.addOnUpdateListener(this)
                arView!!.viewTreeObserver.addOnWindowFocusChangeListener(onFocusListener)
                arView!!.setOnSessionConfigChangeListener(this::onSessionConfigChanged)

                val session = Session(context)
                val config = Config(session)

                imageDatabase = AugmentedImageDatabase(session)
                // Set plane orientation mode
                updatePlaneDetection(config)
                // Enable or not light estimation
                updateLightEstimation(config)
                // Enable or not depth management
                updateDepthManagement(config)

                // Sets the desired focus mode
                config.focusMode = Config.FocusMode.AUTO
                // Force the non-blocking mode for the session.
                config.updateMode = Config.UpdateMode.LATEST_CAMERA_IMAGE
                arView!!.environment?.indirectLight?.intensity = 100000F
                arView!!.environment?.indirectLight?.setRotation(arView!!.environment!!.sphericalHarmonics!!)

                sessionConfig = config
                arSession = session
                arView!!.session?.configure(sessionConfig)
                arView!!.session = arSession

                initializeSession()
                resume()

                // Setup the instructions view.
                instructionsController = InstructionsController(context, this);
                instructionsController!!.setEnabled(isInstructionsEnabled);
            }
        } else {
            isDeviceSupported = false
        }
    }

    private fun resume() {
        if (isStarted) {
            return
        }
        if ((context as ThemedReactContext).currentActivity != null) {
            isStarted = true
            try {
                arView!!.resume()
            } catch (ex: java.lang.Exception) {
                sessionInitializationFailed = true
                returnErrorEvent("Could not resume session")
            }
            if (!sessionInitializationFailed) {
                instructionsController?.setVisible(true)
            }
        }
    }

    /**
     * Initializes the ARCore session. The CAMERA permission is checked before checking the
     * installation state of ARCore. Once the permissions and installation are OK, the method
     * #getSessionConfiguration(Session session) is called to get the session configuration to use.
     * Sceneform requires that the ARCore session be updated using LATEST_CAMERA_IMAGE to avoid
     * blocking while drawing. This mode is set on the configuration object returned from the
     * subclass.
     */
    private fun initializeSession() {
        // Only try once
        if (sessionInitializationFailed) {
            return
        }
        // if we have the camera permission, create the session
        if (CameraPermissionHelper.hasCameraPermission((context as ThemedReactContext).currentActivity)) {
            val sessionException: UnavailableException?
            try {
                onSessionConfigurationListener?.onSessionConfiguration(arSession, sessionConfig)

                postDelayed({
                    // run a JS event
                    Log.d("ARview session", "started")
                    val event = Arguments.createMap()
                    val reactContext = context as ThemedReactContext
                    reactContext.getJSModule(RCTEventEmitter::class.java).receiveEvent(
                        id, "onStarted", event
                    )
                }, 1000)

                return
            } catch (e: UnavailableException) {
                sessionException = e
            } catch (e: java.lang.Exception) {
                sessionException = UnavailableException()
                sessionException.initCause(e)
            }
            sessionInitializationFailed = true
            returnErrorEvent(sessionException?.message)
        } else {
            returnErrorEvent("Missing camera permissions")
        }
    }

    /**
     * Removed the focus listener
     */
    fun onDrop() {
        if (arView != null) {
            arView!!.pause()
            arView!!.session?.close()
            arView!!.destroy()
            arView!!.viewTreeObserver.removeOnWindowFocusChangeListener(onFocusListener)
        }
    }

    /**
     * Occurs when a session configuration has changed.
     */
    private fun onSessionConfigChanged(config: Config) {
        instructionsController?.setEnabled(
            config.planeFindingMode !== Config.PlaneFindingMode.DISABLED
        )
    }

    /**
     * Creates the transformation system used by this view.
     */
    private fun makeTransformationSystem(): TransformationSystem {
        val selectionVisualizer = FootprintSelectionVisualizer()
        return TransformationSystem(resources.displayMetrics, selectionVisualizer)
    }

    /**
     * Makes the transformation system responding to touches
     */
    override fun onPeekTouch(hitTestResult: HitTestResult, motionEvent: MotionEvent?) {
//        transformationSystem!!.onTouch(hitTestResult, motionEvent)
        if (hitTestResult.node != null) {
//            gestureDetector!!.onTouchEvent(motionEvent)
            var node = hitTestResult.node
            while (node !is AnchorNode) {
                node = node!!.parentNode
                if (node == null) break
            }

            if (node == null) return

            val event = Arguments.createMap()
            event.putString("message", (node as AnchorNode).anchor.hashCode().toString())
            val reactContext = context as ThemedReactContext
            reactContext.getJSModule(RCTEventEmitter::class.java).receiveEvent(
                id,
                "onEntityTap",
                event
            )
        }
    }

    /**
     * On each frame
     */
    override fun onUpdate(frameTime: FrameTime?) {
        if (arView!!.session == null || arView!!.arFrame == null) return
        if (instructionsController != null) {
            // Instructions for the Plane finding mode.
            val showInstructions: Boolean = (detectionType == "image" || detectionType == "plane")
                    && !arView!!.hasTrackedAugmentedImage()
                    && !arView!!.hasTrackedPlane()
            if (instructionsController?.isVisible() != showInstructions) {
                instructionsController?.setVisible(showInstructions)
            }
        }

        if (arView!!.arFrame?.camera?.trackingState == TrackingState.TRACKING) {
            // Check if there is already an anchor
//      if (modelNode?.parent is AnchorNode) {
//        return
//      }

            val trackables = arView!!.arFrame?.getUpdatedTrackables(Trackable::class.java)
            if (trackables != null) for (trackable in trackables) {
                val code = trackable.hashCode().toString()
                if (trackable.trackingState == TrackingState.TRACKING
                    && !trackableMap.containsKey(code)
                ) {
                    trackableMap[code] = trackable

                    val event = Arguments.createMap()
                    when (trackable) {
                        is AugmentedImage -> {
                            event.putString("type", "image")
                            event.putString("detected", trackable.name)

                            val anchor = trackable.createAnchor(trackable.centerPose)
                            val id = anchor.hashCode().toString()
                            event.putString("id", id)

                            anchorMap[id] = anchor
                        }

                        is Plane -> {
                            event.putString("type", "plane")
                            event.putString("detected", "")

                            val anchor = trackable.createAnchor(trackable.centerPose)
                            val id = anchor.hashCode().toString()
                            event.putString("id", id)

                            anchorMap[id] = anchor
                        }
                    }

                    val reactContext = context as ThemedReactContext
                    reactContext.getJSModule(RCTEventEmitter::class.java).receiveEvent(
                        id, "onAnchorAdded", event
                    )
                }
            }

            // Create the Anchor.
//      val frame = arView!!.arFrame
//      val metrics = resources.displayMetrics
//      val hitResults = frame!!.hitTest(metrics.widthPixels * .5F, metrics.heightPixels * .5F)
//      if(hitResults.isNotEmpty()) {
//        initAnchorNode(hitResults.first().createAnchor())
//
//        val event = Arguments.createMap()
//        event.putString("type", "plane")
//        val reactContext = context as ThemedReactContext
//        reactContext.getJSModule(RCTEventEmitter::class.java).receiveEvent(
//          id, "onAnchorAdded", event)
//      }
//
//      // tells JS that the model is visible
//      onModelPlaced()
        }
    }

    fun placeModel(id: String, origin: Int) {
        val anchor = anchorMap[id] ?: return
        val scene = arView!!.scene

        if (origin in 0 until modelNodes.size) {
            val model = modelNodes[origin]

            val node = AnchorNode(anchor)
            node.parent = scene
            model.parent = node

            val direction = scene.camera.worldPosition
            val position = node.worldPosition
            direction.x -= position.x
            direction.y = 0F
            direction.z -= position.z

            node.localScale = Vector3(.13F, .13F, .13F)
            model.setLookDirection(direction)
        } else {
            for(node in scene.children) {
                if(node is AnchorNode && node.anchor.hashCode().toString() == id) {
                    scene.removeChild(node)
                    for(child in node.children) node.removeChild(child)
                    return
                }
            }
        }
    }

    fun addAnchor(x: Float, y: Float, z: Float) {
        val cameraPos = arView!!.scene.camera.worldPosition

        val anchor = arView!!.session!!.createAnchor(
            Pose(
                floatArrayOf(
                    cameraPos.x + x,
                    cameraPos.y + y,
                    cameraPos.z + z
                ), floatArrayOf(0f, 0f, 0f, 1f)
            )
        )

        val event = Arguments.createMap()
        val code = anchor.hashCode().toString()
        event.putString("id", code)
        event.putString("type", "world")
        event.putString("detected", "")
        anchorMap[code] = anchor

        val reactContext = context as ThemedReactContext
        reactContext.getJSModule(RCTEventEmitter::class.java).receiveEvent(
            id, "onAnchorAdded", event
        )
    }

//  private fun onModelPlaced() {
//    val event = Arguments.createMap()
//    val reactContext = context as ThemedReactContext
//    reactContext.getJSModule(RCTEventEmitter::class.java).receiveEvent(
//      id,
//      "onModelPlaced",
//      event
//    )
//  }

    /**
     * Request ARCore installation
     */
    private fun requestInstall(): Boolean {
        when (ArCoreApk.getInstance()
            .requestInstall((context as ThemedReactContext).currentActivity, !installRequested)) {
            InstallStatus.INSTALL_REQUESTED -> {
                installRequested = true
                return true
            }

            InstallStatus.INSTALLED -> {}
        }
        return false
    }

    /**
     * Set plane detection orientation
     */
    fun setDetectionType(type: String) {
        detectionType = type
        sessionConfig.let {
            updatePlaneDetection(sessionConfig)
            updateConfig()
        }
    }

    /**
     * Load AR Image
     */
    fun loadArImage(paths: List<String>) {
        val executor = Executors.newSingleThreadExecutor()
        executor.execute {
            // Tries to get the image and post it in the ImageView
            // with the help of Handler
            try {
                Log.i("ARview", "Download start")
                for (path in paths) {
                    val image = if (path.contains("http")) {
                        val `in` = java.net.URL(path).openStream()
                        val img = BitmapFactory.decodeStream(`in`)
                        img
                    } else {
                        val decodedString = Base64.decode(path.substringAfter(","), Base64.DEFAULT)
                        val decodedByte =
                            BitmapFactory.decodeByteArray(decodedString, 0, decodedString.size)
                        decodedByte
                    }

                    // Only for making changes in UI
                    imageDatabase.addImage(path, image, 0.3F)
                }

                Log.i("ARview", "Download finish")
                handler.post {
                    sessionConfig?.augmentedImageDatabase = imageDatabase
                    updateConfig()
                }
            }

            // If the URL doesnot point to
            // image or any other kind of failure
            catch (e: Exception) {
                e.printStackTrace()
                Log.i("ARview", e.toString())
            }
        }
    }

    private fun updatePlaneDetection(config: Config?) {
        when (detectionType) {
            "image" -> {
                config?.planeFindingMode = Config.PlaneFindingMode.DISABLED
                config?.augmentedImageDatabase = imageDatabase
//        if (modelNode != null) {
//          modelNode!!.translationController.allowedPlaneTypes.clear()
//        }
            }

            "plane" -> {
                config?.planeFindingMode = Config.PlaneFindingMode.HORIZONTAL_AND_VERTICAL
                config?.setAugmentedImageDatabase(null)
//        if (modelNode != null) {
//          modelNode!!.translationController.allowedPlaneTypes.clear()
//          modelNode!!.translationController.allowedPlaneTypes = EnumSet.allOf(Plane.Type::class.java)
//        }
            }

            else -> {
                config?.planeFindingMode = Config.PlaneFindingMode.DISABLED
                config?.setAugmentedImageDatabase(null)
//        if (modelNode != null) {
//          modelNode!!.translationController.allowedPlaneTypes.clear()
//        }
            }
        }
    }

    /**
     * Set whether instant placement is enabled
     */
    fun setInstantPlacementEnabled(isEnabled: Boolean) {
        isInstantPlacementEnabled = isEnabled
    }

    /**
     * Set whether light estimation is enabled
     */
    fun setLightEstimationEnabled(isEnabled: Boolean) {
        isLightEstimationEnabled = isEnabled
        sessionConfig.let {
            updateLightEstimation(sessionConfig)
            updateConfig()
        }
    }

    private fun updateLightEstimation(config: Config?) {
        if (!isLightEstimationEnabled) {
            config?.lightEstimationMode = Config.LightEstimationMode.AMBIENT_INTENSITY
        } else {
            config?.lightEstimationMode = Config.LightEstimationMode.ENVIRONMENTAL_HDR
        }
    }

    /**
     * Set whether depth management is enabled
     */
    fun setDepthManagementEnabled(isEnabled: Boolean) {
        isDepthManagementEnabled = isEnabled
        sessionConfig.let {
            updateDepthManagement(sessionConfig)
            updateConfig()
        }
    }

    private fun updateDepthManagement(config: Config?) {
        if (!isDepthManagementEnabled) {
            sessionConfig?.depthMode = Config.DepthMode.DISABLED
            arView?.cameraStream?.depthOcclusionMode =
                CameraStream.DepthOcclusionMode.DEPTH_OCCLUSION_DISABLED
        } else {
            if (arSession?.isDepthModeSupported(Config.DepthMode.AUTOMATIC) == true) {
                sessionConfig?.depthMode = Config.DepthMode.AUTOMATIC
            }
            arView?.cameraStream?.depthOcclusionMode =
                CameraStream.DepthOcclusionMode.DEPTH_OCCLUSION_ENABLED
        }
    }

    private fun updateConfig() {
        if (isStarted) {
            arSession?.configure(sessionConfig)
        }
    }

    /**
     * Start the loading of a GLB model URI
     */
    fun loadModel(src: String) {
        if (isDeviceSupported) {
            Log.d("ARview model", "loading")
            isLoading = true

            ModelRenderable.builder()
                .setSource(context, Uri.parse(src))
                .setIsFilamentGltf(true)
                .build()
                .thenAccept {
                    val modelNode = CustomTransformableNode(transformationSystem!!)
                    modelNode!!.select()
                    modelNode!!.renderable = it
                    // set model at center
                    modelNode!!.renderableInstance.filamentAsset.let { asset ->
                        val center =
                            asset!!.boundingBox.center.let { v -> Float3(v[0], v[1], v[2]) }
                        val halfExtent =
                            asset.boundingBox.halfExtent.let { v -> Float3(v[0], v[1], v[2]) }
                        val fCenter =
                            -(center + halfExtent * Float3(0f, -1f, 1f)) * Float3(1f, 1f, 1f)
                        modelNode!!.localPosition = Vector3(fCenter.x, fCenter.y, fCenter.z)
                    }

                    Log.d("ARview model", "loaded")
                    isLoading = false
                    modelNodes.add(modelNode)
                    lastAnimators.add(null)

                    // set transforms on model
                    onTransformChanged()
                }
                .exceptionally {
                    Log.e("ARview model", "cannot load")
                    returnErrorEvent("Cannot load the model: " + it.message)
                    return@exceptionally null
                }
        }
    }

    /**
     * Rotate the model with the requested angle
     */
//  fun rotateModel(pitch: Number, yaw: Number, roll:Number) {
//    Log.d("ARview rotateModel", "pitch: $pitch deg / yaw: $yaw deg / roll: $roll deg")
//    modelNode?.localRotation = Quaternion.multiply(modelNode?.localRotation, Quaternion.eulerAngles(Vector3(pitch.toFloat(), yaw.toFloat(), roll.toFloat())))
//  }

    /**
     * Remove the model from the view and reset plane detection
     */
    fun reset() {
        Log.d("ARview model", "Resetting model")
        arView?.scene?.children?.clear()
        trackableMap.clear()
    }

    fun playAnimation(origin: Int, id: Int, count: Int) {
        // Animate if has animation
        Log.d("ARview", "playAnimation")
        val renderableInstance = modelNodes[origin].renderableInstance

        lastAnimators[origin]?.end()
        if (renderableInstance != null && renderableInstance.hasAnimations()) {
            val animator = renderableInstance.animate(id)
            animator.repeatCount = count
            animator.start()

            lastAnimators[origin] = animator
        }
    }

    /**
     * Add a transformation to the allowed list
     */
    fun addAllowTransform(transform: String) {
        allowTransform.add(transform)
        onTransformChanged()
    }

    /**
     * Remove a transformation to the allowed list
     */
    fun removeAllowTransform(transform: String) {
        allowTransform.remove(transform)
        onTransformChanged()
    }

    private fun onTransformChanged() {
//    if (modelNode == null) return
//    modelNode!!.scaleController.isEnabled = allowTransform.contains("scale")
//    modelNode!!.rotationController.isEnabled = allowTransform.contains("rotate")
//    modelNode!!.translationController.isEnabled = allowTransform.contains("translate")
    }

    /**
     * Enable/Disable instructions
     */
    fun setInstructionsEnabled(isEnabled: Boolean) {
        isInstructionsEnabled = isEnabled
        instructionsController?.setEnabled(isInstructionsEnabled)
    }

    /**
     * Takes a screenshot of the view and send it to JS through event
     */
    fun takeScreenshot(requestId: Int) {
        Log.d("ARview takeScreenshot", requestId.toString())

        val bitmap = Bitmap.createBitmap(
            width, height,
            Bitmap.Config.ARGB_8888
        )
        var encodedImage: String? = null
        var encodedImageError: String? = null
        PixelCopy.request(arView!!, bitmap, { copyResult ->
            if (copyResult == PixelCopy.SUCCESS) {
                try {
                    val byteArrayOutputStream = ByteArrayOutputStream()
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 70, byteArrayOutputStream)
                    val byteArray = byteArrayOutputStream.toByteArray()
                    val encoded = Base64.encodeToString(byteArray, Base64.DEFAULT)
                    encodedImage = encoded
                    Log.d("ARview takeScreenshot", "success")
                } catch (e: Exception) {
                    encodedImageError = "The image cannot be saved: " + e.localizedMessage
                    Log.d("ARview takeScreenshot", "fail")
                }
                returnDataEvent(requestId, encodedImage, encodedImageError)
            }
        }, Handler(Looper.getMainLooper()))
    }

    /**
     * Send back an event to JS
     */
    private fun returnDataEvent(requestId: Int, result: String?, error: String?) {
        val event = Arguments.createMap()
        event.putString("requestId", requestId.toString())
        event.putString("result", result)
        event.putString("error", error)
        val reactContext = context as ThemedReactContext
        reactContext.getJSModule(RCTEventEmitter::class.java).receiveEvent(
            id,
            "onDataReturned",
            event
        )
    }

    /**
     * Send back an error event to JS
     */
    private fun returnErrorEvent(message: String?) {
        val event = Arguments.createMap()
        event.putString("message", message)
        val reactContext = context as ThemedReactContext
        reactContext.getJSModule(RCTEventEmitter::class.java).receiveEvent(
            id,
            "onError",
            event
        )
    }

    /**
     * Returns false and displays an error message if Sceneform can not run, true if Sceneform can run
     * on this device.
     *
     *
     * Sceneform requires Android N on the device as well as OpenGL 3.0 capabilities.
     *
     *
     * Finishes the activity if Sceneform can not run
     */
    private fun checkIsSupportedDevice(activity: Activity): Boolean {
        val openGlVersionString =
            (activity.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager)
                .deviceConfigurationInfo
                .glEsVersion
        if (openGlVersionString.toDouble() < 3.0) {
            returnErrorEvent("This feature requires OpenGL ES 3.0 later")
            return false
        }
        return true
    }
}


