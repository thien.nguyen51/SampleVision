package com.reactnativearviewer

import android.util.Log
import androidx.annotation.Nullable
import com.facebook.react.bridge.ReadableArray
import com.facebook.react.common.MapBuilder
import com.facebook.react.uimanager.SimpleViewManager
import com.facebook.react.uimanager.ThemedReactContext
import com.facebook.react.uimanager.annotations.ReactProp

class ArViewerViewManager : SimpleViewManager<ArViewerView>() {
  /**
   * Assign an identifier to each command supported
   */
  companion object {
    const val COMMAND_SNAPSHOT = 1
    const val COMMAND_CHANGE_DETECTION = 2
    const val COMMAND_LOAD_IMAGE = 3
    const val COMMAND_RESET = 4
    const val COMMAND_PLAY_ANIMATION = 5
    const val COMMAND_LOAD_MODEL = 6
    const val COMMAND_PLACE_MODEL = 7
    const val COMMAND_ADD_ANCHOR = 8
  }

  /**
   * Name the view
   */
  override fun getName() = "ArViewerView"

  /**
   * Create the view
   */
  override fun createViewInstance(reactContext: ThemedReactContext): ArViewerView {
    Log.d("ARview createViewInstance", "Create view")
    return ArViewerView(reactContext)
  }

  /**
   * Pause the AR session when the view gets removed
   */
  override fun onDropViewInstance(view: ArViewerView) {
    Log.d("ARview onDropViewInstance", "Stopping session")
    super.onDropViewInstance(view)
    view.onDrop()
  }

  /**
   * Map the commands to an integer
   */
  override fun getCommandsMap(): Map<String, Int>? {
    return mapOf(
      "takeScreenshot" to COMMAND_SNAPSHOT,
      "changeDetectionType" to COMMAND_CHANGE_DETECTION,
      "loadArImage" to COMMAND_LOAD_IMAGE,
      "reset" to COMMAND_RESET,
      "playAnimation" to COMMAND_PLAY_ANIMATION,
      "loadModel" to COMMAND_LOAD_MODEL,
      "placeModel" to COMMAND_PLACE_MODEL,
      "addAnchor" to COMMAND_ADD_ANCHOR,
    )
  }

  /**
   * Map methods calls to view methods
   */
  override fun receiveCommand(view: ArViewerView, commandId: Int, @Nullable args: ReadableArray?) {
    super.receiveCommand(view, commandId, args)
    Log.d("ARview receiveCommand", commandId.toString())
    when (commandId) {
      COMMAND_SNAPSHOT -> if (args != null) {
        val requestId = args.getInt(0)
        view.takeScreenshot(requestId)
      }

      COMMAND_CHANGE_DETECTION -> if (args != null) {
        view.setDetectionType(args.getString(0))
      }

      COMMAND_LOAD_IMAGE -> if (args != null) {
        val arr = args.getArray(0)
        val list = mutableListOf<String>()
        for (i in 0 until arr.size()) {
          list.add(arr.getString(i))
        }

//          Log.i("Thien", list.size.toString())
        view.loadArImage(list)
      }

      COMMAND_RESET -> {
        view.reset()
      }

      COMMAND_PLAY_ANIMATION -> if (args != null) {
        val origin = args.getInt(0)
        val id = args.getInt(1)
        val count = args.getInt(2)
        view.playAnimation(origin, id, count - 1)
      }

      COMMAND_LOAD_MODEL -> if (args != null) {
        val arr = args.getArray(0)
        val list = mutableListOf<String>()
        for (i in 0 until arr.size()) {
          list.add(arr.getString(i))
        }

//          Log.i("Thien", list.size.toString())
        view.loadModel(list[0])
      }

      COMMAND_PLACE_MODEL -> if(args != null) {
        val id = args.getString(0)
        val origin = args.getInt(1)
        view.placeModel(id, origin)
      }

      COMMAND_ADD_ANCHOR -> if(args != null) {
        val x = args.getDouble(0).toFloat()
        val y = args.getDouble(1).toFloat()
        val z = args.getDouble(2).toFloat()
        view.addAnchor(x, y, z)
      }
    }
  }

  /**
   * Register the view events
   */
  override fun getExportedCustomDirectEventTypeConstants(): MutableMap<String, Any> {
    return MapBuilder.of(
      "onDataReturned", MapBuilder.of("registrationName","onDataReturned"),
      "onError", MapBuilder.of("registrationName","onError"),
      "onStarted", MapBuilder.of("registrationName","onStarted"),
      "onEnded", MapBuilder.of("registrationName","onEnded"),
      "onAnchorAdded", MapBuilder.of("registrationName","onAnchorAdded"),
      "onEntityTap", MapBuilder.of("registrationName","onEntityTap")
    )
  }

  /**
   * Required prop: the model src (URI)
   */
  @ReactProp(name = "model")
  fun setModel(view: ArViewerView, model: String) {
    Log.d("ARview model", model)
    view.loadModel(model)
  }

  /**
   * Optional: the plane orientation detection (can be: horizontal, vertical, both, none)
   */
  @ReactProp(name = "detectionType")
  fun setDetectionType(view: ArViewerView, type: String) {
    Log.d("ARview detectionType", type)
    view.setDetectionType(type)
  }

  /**
   * Optional: enable ARCode light estimation
   */
  @ReactProp(name = "lightEstimation")
  fun setLightEstimation(view: ArViewerView, lightEstimation: Boolean) {
    Log.d("ARview lightEstimation", lightEstimation.toString())
    view.setLightEstimationEnabled(lightEstimation)
  }


  /**
   * Optional: enable SceneView depth management
   */
  @ReactProp(name = "manageDepth")
  fun setManageDepth(view: ArViewerView, manageDepth: Boolean) {
    Log.d("ARview manageDepth", manageDepth.toString())
    view.setDepthManagementEnabled(manageDepth)
  }


  /**
   * Optional: allow user to pinch the model to zoom it
   */
  @ReactProp(name = "allowScale")
  fun setAllowScale(view: ArViewerView, allowScale: Boolean) {
    Log.d("ARview allowScale", allowScale.toString())
    if (allowScale) {
      view.addAllowTransform("scale")
    } else {
      view.removeAllowTransform("scale")
    }
  }

  /**
   * Optional: allow user to translate the model
   */
  @ReactProp(name = "allowTranslate")
  fun setAllowTranslate(view: ArViewerView, allowTranslate: Boolean) {
    Log.d("ARview allowTranslate", allowTranslate.toString())
    if(allowTranslate) {
      view.addAllowTransform("translate")
    } else {
      view.removeAllowTransform("translate")
    }
  }

  /**
   * Optional: allow the user to rotate the model
   */
  @ReactProp(name = "allowRotate")
  fun setAllowRotate(view: ArViewerView, allowRotate: Boolean) {
    Log.d("ARview allowRotate", allowRotate.toString())
    if(allowRotate) {
      view.addAllowTransform("rotate")
    } else {
      view.removeAllowTransform("rotate")
    }
  }

  /**
   * Optional: disable the text instructions
   */
  @ReactProp(name = "disableInstructions")
  fun disableInstructions(view: ArViewerView, isDisabled: Boolean) {
    Log.d("ARview setInstructions", isDisabled.toString())
    view.setInstructionsEnabled(!isDisabled)
  }

  /**
   * Optional: disable instant placement
   */
  @ReactProp(name = "disableInstantPlacement")
  fun disableInstantPlacement(view: ArViewerView, isDisabled: Boolean) {
    Log.d("ARview disableInstantPlacement", isDisabled.toString())
    view.setInstantPlacementEnabled(!isDisabled)
  }
}
