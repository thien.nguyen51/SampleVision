import * as React from 'react';

import { Button, Image, View } from 'react-native';
import { ArViewerView } from 'react-native-ar-viewer';

export default function App() {
  const ref = React.useRef() as React.MutableRefObject<ArViewerView>;
  const [detected, setDetected] = React.useState<string>(' ')

  const loadImage = () => {
    ref.current && ref.current.loadArImage([
      'https://static.momocdn.net/app/img/Gamification/lacxi2023/IMG_0709.jpg',
      'https://static.momocdn.net/app/img/Gamification/lacxi2023/IMG_0117.jpg',
      // 'https://static.momocdn.net/app/img/Gamification/lacxi2023/IMG_0713.jpg',
      'https://static.momocdn.net/app/img/Gamification/lacxi2023/IMG_0715.jpg',
      'https://static.momocdn.net/app/img/Gamification/lacxi2023/IMG_0716.jpg',
    ])
  }

  const takeScreenshot = async () => {
    console.log('takeScreenshot')
    let res = await ref.current.takeScreenshot()
    setDetected('data:image/jpeg;base64,' + res)
    console.log(res)
  }

  return <>
    <ArViewerView
      style={{ flex: 1 }}
      detectionType='image'
      // manageDepth
      onStarted={loadImage}
      onEnded={() => console.log('ended')}
      onAnchorAdded={(e) => setDetected(e.nativeEvent.detected)}
      ref={ref}
    />
    {/* <Button onPress={takeScreenshot} title="Take Screenshot" /> */}
    {detected.length > 1 && <View style={{
      position: 'absolute',
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#ffffff',
      width: 200,
      height: 300,
      borderRadius: 10,
      left: 100, top: 100
    }}>
      <Image source={{ uri: detected, width: 100, height: 100 }} />
    </View>}
  </>
}
