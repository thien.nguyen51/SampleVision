import { Component, RefObject, SyntheticEvent } from 'react';
import { ViewStyle, HostComponent } from 'react-native';
type ArEvent = SyntheticEvent<{}, {
    requestId: number | string;
    result: string;
    error: string;
}>;
type ArErrorEvent = SyntheticEvent<{}, {
    message: string;
}>;
type ArStatelessEvent = SyntheticEvent<{}, {
    detected: string;
    type: string;
    id: string;
}>;
type DetectionType = 'none' | 'plane' | 'image';
type ArViewerProps = {
    model?: string;
    detectionType?: DetectionType;
    allowScale?: boolean;
    allowRotate?: boolean;
    allowTranslate?: boolean;
    lightEstimation?: boolean;
    manageDepth?: boolean;
    disableInstructions?: boolean;
    disableInstantPlacement?: boolean;
    style?: ViewStyle;
    ref?: RefObject<HostComponent<ArViewerProps> | (() => never)>;
    onDataReturned: (e: ArEvent) => void;
    onError?: (e: ArErrorEvent) => void | undefined;
    onStarted?: (e: ArStatelessEvent) => void | undefined;
    onEnded?: (e: ArStatelessEvent) => void | undefined;
    onAnchorAdded?: (e: ArStatelessEvent) => void | undefined;
    onEntityTap?: (e: ArErrorEvent) => void | undefined;
};
type ArInnerViewProps = Omit<ArViewerProps, 'onDataReturned' | 'ref' | 'onError'>;
type ArInnerViewState = {
    cameraPermission: boolean;
};
export declare class ArViewerView extends Component<ArInnerViewProps, ArInnerViewState> {
    private _nextRequestId;
    private _requestMap;
    private nativeRef;
    constructor(props: ArInnerViewProps);
    componentDidMount(): void;
    _onDataReturned(event: ArEvent): void;
    _onError(event: ArErrorEvent): void;
    /**
     * Takes a full screenshot of the rendered camera
     * @returns A promise resolving a base64 encoded image
     */
    takeScreenshot(): Promise<string>;
    /**
     * Change the detection type
     * @returns void
     */
    changeDetectionType(type: DetectionType): void;
    /**
     * Load Ar Image
     * @returns void
     */
    loadArImage(urls: string[]): void;
    /**
     * Restart session
     * @returns void
     */
    reset(): void;
    /**
     * Play Animation
     * @returns void
     */
    playAnimation(origin: number, id: number, count: number): void;
    /**
     * Load model with animation
     * @returns void
     */
    loadModel(urls: string[]): void;
    /**
     * Place model at anchor
     * @returns void
     */
    placeModel(id: string, origin: number): void;
    /**
     * Add anchor at x, y, z
     * @returns void
     */
    addAnchor(x: number, y: number, z: number): void;
    render(): false | JSX.Element;
}
export {};
//# sourceMappingURL=index.d.ts.map