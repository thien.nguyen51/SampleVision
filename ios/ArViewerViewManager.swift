import ARKit
import RealityKit
import SwiftUI

@available(iOS 13.0, *)
@objc(ArViewerViewManager)
class ArViewerViewManager: RCTViewManager {
    @objc override static func requiresMainQueueSetup() -> Bool {
        return false
    }
        
    override func view() -> UIView {
        return ArViewerView()
    }
    
    @objc func changeDetectionType(_ node : NSNumber, withType type: NSString){
        RCTExecuteOnMainQueue {
            if let view = self.bridge.uiManager.view(forReactTag: node) as? ArViewerView {
                view.detectionType = type as String
            }
        }
    }
    
    @objc func takeScreenshot(_ node : NSNumber, withRequestId requestId: NSNumber){
        RCTExecuteOnMainQueue {
            if let view = self.bridge.uiManager.view(forReactTag: node) as? ArViewerView {
                view.takeScreenshot(requestId: requestId.intValue)
            }
        }
    }
    
    @objc func loadArImage(_ node : NSNumber, withURLs urls: NSArray){
        RCTExecuteOnMainQueue {
            if let view = self.bridge.uiManager.view(forReactTag: node) as? ArViewerView {
                view.loadArImage(urls: urls as! Array<String>)
            }
        }
    }
    
    @objc func reset(_ node : NSNumber){
        RCTExecuteOnMainQueue {
            if let view = self.bridge.uiManager.view(forReactTag: node) as? ArViewerView {
                view.reset()
            }
        }
    }
    
    @objc func playAnimation(_ node : NSNumber, forOrigin origin: NSNumber, withId index: NSNumber, andCount count: NSNumber){
        RCTExecuteOnMainQueue {
            if let view = self.bridge.uiManager.view(forReactTag: node) as? ArViewerView {
                view.playAnimation(origin: origin as! Int, id: index as! Int, count: count as! Int)
            }
        }
    }
    
    @objc func loadModel(_ node : NSNumber, withURLs urls: NSArray){
        RCTExecuteOnMainQueue {
            if let view = self.bridge.uiManager.view(forReactTag: node) as? ArViewerView {
                view.loadModel(urls: urls as! Array<String>)
            }
        }
    }
    
    @objc func addAnchor(_ node : NSNumber, atX x: NSNumber, atY y: NSNumber, atZ z: NSNumber){
        RCTExecuteOnMainQueue {
            if let view = self.bridge.uiManager.view(forReactTag: node) as? ArViewerView {
                view.addAnchor(x: x as! Float, y: y as! Float, z: z as! Float)
            }
        }
    }
    
    @objc func placeModel(_ node : NSNumber, forId id: NSString, withOrigin origin: NSNumber){
        RCTExecuteOnMainQueue {
            if let view = self.bridge.uiManager.view(forReactTag: node) as? ArViewerView {
                view.placeModel(id: id as String, origin: origin as! Int)
            }
        }
    }
}


extension UIView {
    var parentViewController: UIViewController? {
        var parentResponder: UIResponder? = self
        while parentResponder != nil {
            parentResponder = parentResponder!.next
            if let viewController = parentResponder as? UIViewController {
                return viewController
            }
        }
        return nil
    }
}
