import RealityKit

class RealityKitViewController: UIViewController {
    @IBOutlet var arView: ModelARView!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if (arView == nil) {
            arView = ModelARView(frame: view.frame)
        }
        view.addSubview(arView)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        // pause session on view disappear
        arView.pause()
        arView.session.delegate = nil
        arView.scene.anchors.removeAll()
        arView.removeFromSuperview()
        arView = nil
    }
    
    func setUp() {
        arView.setUp()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        //1. Get The Current Touch Location & Perform An ARSCNHitTest To Check For Any Hit SCNNode's
        
        print("touchesBegan")
        
        guard let currentTouchLocation = touches.first?.location(in: self.arView),
              let hitTestNode = self.arView.entity(at:currentTouchLocation) else { return }

        var node = hitTestNode.parent
        while (node?.parent != nil) {
            print(node?.name ?? "")
            node = node?.parent
        }
        
        print(String(node?.id ?? 0))
        self.arView.onEntityTap(["message":String(node?.id ?? 0)])
    }
    
    func loadEntity(src: String) -> Entity? {
        // load the model
        let url = URL(fileURLWithPath: src)
        if let modelEntity = try? ModelEntity.load(contentsOf: url) { return modelEntity; }
        
        // Create a new alert
        let dialogMessage = UIAlertController(title: "Error", message: "Cannot load the requested model file.", preferredStyle: .alert)
        dialogMessage.addAction(UIAlertAction(title: "Ok", style: .default, handler: { _ in }))
        // Present alert to user
        self.present(dialogMessage, animated: true, completion: nil)
        return nil
    }
    
    func loadEntity2(srcList: Array<String>) -> Array<Entity?> {
        // load the model
        let entities = srcList.map{loadEntity(src:$0)}
        
        return entities
    }
    
    func changeDetectionType(type: String) {
        arView.changeDetectionType(type: type)
    }
    
    func changeInstructionVisibility(isVisible: Bool) {
        arView.setInstructionsVisibility(isVisible: isVisible)
    }
    
    func changeModel(models: Array<String>) {
        if(models.isEmpty || models[0].isEmpty) { return }
        
        let entities = loadEntity2(srcList: models)
//        arView.changeEntity(modelEntities: entities)
        
        if let model = entities[0] {
            model.setScale([0.1, 0.1, 0.1], relativeTo: nil)
            model.generateCollisionShapes(recursive: true)
        }
        
        arView.modelEntities.append(contentsOf: entities)
    }
    
    func changeLightEstimationEnabled(isEnabled: Bool) {
        arView.setLightEstimationEnabled(isEnabled: isEnabled)
    }
    
    func changeDepthManagementEnabled(isEnabled: Bool) {
        arView.setDepthManagement(isEnabled: isEnabled)
    }
    
    func changeInstantPlacementEnabled(isEnabled: Bool) {
        arView.setInstantPlacementEnabled(isEnabled: isEnabled)
    }
    
    func changeAllowRotate(isAllowed: Bool) {
        arView.setAllowRotate(isEnabled: isAllowed)
    }
    
    func changeAllowTranslate(isAllowed: Bool) {
        arView.setAllowTranslate(isEnabled: isAllowed)
    }
    
    func changeAllowScale(isAllowed: Bool) {
        arView.setAllowScale(isEnabled: isAllowed)
    }
    
    func run() {
        arView.readyToStart = true
        arView.start()
    }
    
    /// Set on started event
    func setOnStartedHandler(handler: @escaping RCTDirectEventBlock) {
        arView.setOnStartedHandler(handler: handler)
    }
    
    /// Set on error event
    func setOnErrorHandler(handler: @escaping RCTDirectEventBlock) {
        arView.setOnErrorHandler(handler: handler)
    }
    
    /// Set on data returned handler (used to resolve promises on JS side)
    func setOnDataReturnedHandler(handler: @escaping RCTDirectEventBlock) {
        arView.setOnDataReturnedHandler(handler: handler)
    }
    
    /// Set on data returned handler (used to resolve promises on JS side)
    func setOnEndedHandler(handler: @escaping RCTDirectEventBlock) {
        arView.setOnEndedHandler(handler: handler)
    }
    
    /// Set on data returned handler (used to resolve promises on JS side)
    func setOnAnchorAddedHandler(handler: @escaping RCTDirectEventBlock) {
        arView.setOnAnchorAddedHandler(handler: handler)
    }
    
    /// Set on data returned handler (used to resolve promises on JS side)
    func setOnEntityTapHandler(handler: @escaping RCTDirectEventBlock) {
        arView.setOnEntityTapHandler(handler: handler)
    }
}
