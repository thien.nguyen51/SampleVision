#import "React/RCTViewManager.h"

@interface RCT_EXTERN_MODULE(ArViewerViewManager, RCTViewManager)

RCT_EXPORT_VIEW_PROPERTY(model, NSString)
RCT_EXPORT_VIEW_PROPERTY(detectionType, NSString)
RCT_EXPORT_VIEW_PROPERTY(allowScale, BOOL)
RCT_EXPORT_VIEW_PROPERTY(allowRotate, BOOL)
RCT_EXPORT_VIEW_PROPERTY(allowTranslate, BOOL)
RCT_EXPORT_VIEW_PROPERTY(lightEstimation, BOOL)
RCT_EXPORT_VIEW_PROPERTY(manageDepth, BOOL)
RCT_EXPORT_VIEW_PROPERTY(disableInstructions, BOOL)
RCT_EXPORT_VIEW_PROPERTY(disableInstantPlacement, BOOL)

RCT_EXTERN_METHOD(changeDetectionType:(nonnull NSNumber*) reactTag withType:(nonnull NSString*)type)
RCT_EXTERN_METHOD(takeScreenshot:(nonnull NSNumber*)reactTag withRequestId:(nonnull NSNumber*)requestId)
RCT_EXTERN_METHOD(reset:(nonnull NSNumber*)reactTag)
RCT_EXTERN_METHOD(playAnimation:(nonnull NSNumber*)reactTag forOrigin:(nonnull NSNumber*)origin withId:(nonnull NSNumber*)index andCount:(nonnull NSNumber*)count)
RCT_EXTERN_METHOD(loadArImage:(nonnull NSNumber*)reactTag withURLs:(nonnull NSArray*)urls)
RCT_EXTERN_METHOD(loadModel:(nonnull NSNumber*)reactTag withURLs:(nonnull NSArray*)urls)
RCT_EXTERN_METHOD(placeModel:(nonnull NSNumber*)reactTag forId:(nonnull NSString*)id withOrigin:(nonnull NSNumber*)origin)
RCT_EXTERN_METHOD(addAnchor:(nonnull NSNumber*)reactTag atX:(nonnull NSNumber*)x atY:(nonnull NSNumber*)y atZ:(nonnull NSNumber*)z)

RCT_EXPORT_VIEW_PROPERTY(onStarted, RCTDirectEventBlock)
RCT_EXPORT_VIEW_PROPERTY(onError, RCTDirectEventBlock)
RCT_EXPORT_VIEW_PROPERTY(onDataReturned, RCTDirectEventBlock)
RCT_EXPORT_VIEW_PROPERTY(onEnded, RCTDirectEventBlock)
RCT_EXPORT_VIEW_PROPERTY(onAnchorAdded, RCTDirectEventBlock)
RCT_EXPORT_VIEW_PROPERTY(onEntityTap, RCTDirectEventBlock)

@end

