import SwiftUI

class ArViewerView: UIView {

    var arViewController: RealityKitViewController!

    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    public override func layoutSubviews() {
        super.layoutSubviews()

        if arViewController == nil {
            // setup subview
            guard let parentViewController = parentViewController else { return }

            arViewController = RealityKitViewController()
            arViewController.view.frame = bounds
            parentViewController.addChild(arViewController)
            addSubview(arViewController.view)
            arViewController.didMove(toParent: parentViewController)
            
            arViewController.setUp()
            // re-run all setters now that the view is mounted
            arViewController.changeDetectionType(type: detectionType)
            arViewController.changeInstructionVisibility(isVisible: !disableInstructions)
            arViewController.changeLightEstimationEnabled(isEnabled: lightEstimation)
            arViewController.changeDepthManagementEnabled(isEnabled: manageDepth)
            arViewController.changeInstantPlacementEnabled(isEnabled: !disableInstantPlacement)
            arViewController.changeAllowScale(isAllowed: allowScale)
            arViewController.changeAllowRotate(isAllowed: allowRotate)
            arViewController.changeAllowTranslate(isAllowed: allowTranslate)
            arViewController.changeModel(models: [model])
            // set events
            if (onError != nil) {
                arViewController.setOnErrorHandler(handler: onError!)
            }
            if (onStarted != nil) {
                arViewController.setOnStartedHandler(handler: onStarted!)
            }
            if (onDataReturned != nil) {
                arViewController.setOnDataReturnedHandler(handler: onDataReturned!)
            }
            if (onEnded != nil) {
                arViewController.setOnEndedHandler(handler: onEnded!)
            }
            if (onAnchorAdded != nil) {
                arViewController.setOnAnchorAddedHandler(handler: onAnchorAdded!)
            }
            if (onEntityTap != nil) {
                arViewController.setOnEntityTapHandler(handler: onEntityTap!)
            }
            // and start session
            arViewController.run()
        } else {
            // update frame
            arViewController?.view.frame = bounds
        }
    }
    
    // reset
    @objc func reset() -> Void {
        arViewController?.arView.reset()
    }
    
    // take a snapshot
    @objc func takeScreenshot(requestId: Int) -> Void {
        arViewController?.arView.takeSnapshot(requestId: requestId)
    }
    
    // load ar image into database
    @objc func loadArImage(urls: Array<String>) -> Void {
        arViewController?.arView.downloadImages(from: urls)
    }
    
    @objc func playAnimation(origin: Int, id: Int, count: Int) -> Void {
        if let models = arViewController?.arView.modelEntities {
            guard let data = models[id]?.availableAnimations[0] else { return }
            
            models[origin]?.playAnimation(data.repeat(count: count), transitionDuration: TimeInterval(0), startsPaused: false)
        }
    }
    
    @objc func loadModel(urls: Array<String>) -> Void {
        arViewController?.changeModel(models: urls)
    }
    
    @objc func placeModel(id: String, origin: Int) -> Void {
        arViewController?.arView.placeModel(id: id, origin: origin)
    }
    
    @objc func addAnchor(x: Float, y: Float, z: Float) -> Void {
        arViewController?.arView.addAnchor(x: x, y: y, z: z, name: "worldAnchor")
    }
    
    /// Remind that properties can be set before the view has been initialized
    @objc var model: String = "" {
      didSet {
          arViewController?.changeModel(models: [model])
      }
    }
    
    @objc var detectionType: String = "" {
      didSet {
          arViewController?.changeDetectionType(type: detectionType)
      }
    }
    
    @objc var disableInstructions: Bool = false {
      didSet {
          arViewController?.changeInstructionVisibility(isVisible: !disableInstructions)
      }
    }
    
    @objc var lightEstimation: Bool = false {
      didSet {
          arViewController?.changeLightEstimationEnabled(isEnabled: lightEstimation)
      }
    }
    
    @objc var manageDepth: Bool = false {
      didSet {
          arViewController?.changeDepthManagementEnabled(isEnabled: manageDepth)
      }
    }
    
    @objc var disableInstantPlacement: Bool = false {
      didSet {
          arViewController?.changeInstantPlacementEnabled(isEnabled: !disableInstantPlacement)
      }
    }
    
    @objc var allowRotate: Bool = false {
      didSet {
          arViewController?.changeAllowRotate(isAllowed: allowRotate)
      }
    }
    
    @objc var allowScale: Bool = false {
      didSet {
          arViewController?.changeAllowScale(isAllowed: allowScale)
      }
    }
    
    @objc var allowTranslate: Bool = false {
      didSet {
          arViewController?.changeAllowTranslate(isAllowed: allowTranslate)
      }
    }
    
    @objc var onStarted: RCTDirectEventBlock? {
        didSet {
            arViewController?.setOnStartedHandler(handler: onStarted!)
        }
    }
    
    @objc var onDataReturned: RCTDirectEventBlock? {
        didSet {
            arViewController?.setOnDataReturnedHandler(handler: onDataReturned!)
        }
    }
    
    @objc var onError: RCTDirectEventBlock? {
        didSet {
            arViewController?.setOnErrorHandler(handler: onError!)
        }
    }
    
    @objc var onEnded: RCTDirectEventBlock? {
        didSet {
            arViewController?.setOnEndedHandler(handler: onError!)
        }
    }
    
    @objc var onAnchorAdded: RCTDirectEventBlock? {
        didSet {
            arViewController?.setOnAnchorAddedHandler(handler: onError!)
        }
    }
    
    @objc var onEntityTap: RCTDirectEventBlock? {
        didSet {
            arViewController?.setOnEntityTapHandler(handler: onError!)
        }
    }
}
