import ARKit
import RealityKit

@available(iOS 13.0, *)
class ModelARView: ARView, ARSessionDelegate {
    var modelEntities = [Entity?]()
    var config: ARWorldTrackingConfiguration!
    var arImages: Set<ARReferenceImage>?
//    var grids = [Grid]()
    var isModelVisible: Bool = false {
        didSet {
//            if (isModelVisible && self.onModelPlacedHandler != nil) {
//                self.onModelPlacedHandler([:])
//            } else if(!isModelVisible && self.onModelRemovedHandler != nil) {
//                self.onModelRemovedHandler([:])
//            }
        }
    }
    var coachingOverlay: ARCoachingOverlayView!
    var isInstantPlacementEnabled: Bool = true
    var allowedGestures: ARView.EntityGestures = []
    var installedGestureRecognizers: [EntityGestureRecognizer] = []
    var isSetup: Bool = false
    var readyToStart: Bool = false
    var sessionStarted: Bool = false
    
    var onStartedHandler: RCTDirectEventBlock!
    var onErrorHandler: RCTDirectEventBlock!
    var onDataReturnedHandler: RCTDirectEventBlock!
    var onEndedHandler: RCTDirectEventBlock!
    var onAnchorAdded: RCTDirectEventBlock!
    var onEntityTap: RCTDirectEventBlock!
    
    required init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required dynamic init?(coder decoder: NSCoder) {
        super.init(coder: decoder)
    }
    
    /// Setup the view
    func setUp() {
        // manage orientation change
        self.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        renderOptions.insert(.disableAREnvironmentLighting)
        // Add coaching overlay
        coachingOverlay = ARCoachingOverlayView(frame: frame)
        // setup the instructions
        coachingOverlay.goal = .anyPlane
        coachingOverlay.setActive(false, animated: true)
        coachingOverlay.session = self.session
        // Make sure it rescales if the device orientation changes
        coachingOverlay.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        // update frame
        coachingOverlay.frame = self.frame
        self.addSubview(coachingOverlay)
        
        let config = ARWorldTrackingConfiguration()
        
        arImages = Set<ARReferenceImage>()
        config.isLightEstimationEnabled = false

        if #available(iOS 13.4, *) {
            if ARWorldTrackingConfiguration.supportsSceneReconstruction(.mesh) {
                config.sceneReconstruction = .mesh
            }
        }
        
        // load environment lighting from HDR file
        let frameworkBundle = Bundle(for: ArViewerViewManager.self)
        let bundleURL = frameworkBundle.resourceURL?.appendingPathComponent("ArViewerBundle.bundle")
        let resourceBundle = Bundle(url: bundleURL!)
        
        do {
            let skyboxResource = try EnvironmentResource.load(named: "ref", in: resourceBundle)
            environment.lighting.resource = skyboxResource
            environment.lighting.intensityExponent = 1
        } catch {
            let message = "Cannot load environment texture, please check installation guide. Models may appear darker than expected."
            print(message)
        }
        
        self.config = config
        
        // manage session here
        self.session.delegate = self
        
//        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
//        tap.name = "gridTap"
//        self.addGestureRecognizer(tap)
        
        isSetup = true
    }
    
    /// Hide all grids and display the model on the provided one
//    func showModel(grid: Grid, model: Entity) {
//        for gr in grids {
//            if (gr != grid) {
//                gr.isEnabled = false
//            }
//        }
//
//        grid.isEnabled = true
//        grid.replaceModel(model: modelEntities[0]!)
//        isModelVisible = true
//        setGestures()
//    }
    
    /// Reset the views and all grids
    func reset() {
//        for grid in self.grids {
//            grid.reset()
//            grid.isEnabled = true
//        }
        isModelVisible = false
//        setGestures()
//        self.grids.removeAll()
        self.scene.anchors.removeAll()
        start()
    }
    
    /// Start or update the AR session
    func start() {
        if (isSetup && readyToStart) {
//            NSLog("Thien " + String(arImages?.count ?? 0))
            self.session.run(self.config, options: [.removeExistingAnchors, .resetSceneReconstruction, .resetTracking, .stopTrackedRaycasts])
            if(!sessionStarted) {
                sessionStarted = true
                if (onStartedHandler != nil) {
                    onStartedHandler([:])
                }
            }
        }
    }
    
    
    /// Pause the AR session
    func pause() {
        self.session.pause()
        sessionStarted = false
        if (onEndedHandler != nil) {
            onEndedHandler([:])
        }
    }
    
    func removeImages(from urls: Array<String>) {
        for url in urls {
            let id = arImages?.firstIndex(where: { $0.name == url })
            if(id != nil) { arImages?.remove(at: id!) }
        }
    }
    
    func downloadImages(from urls: Array<String>) {
//        print("Download Started")
        Task {
            for url in urls {
                let request = URLRequest(url: URL(string: url)!)
                let (data, _) = try! await URLSession.shared.data(for: request)
                let image = UIImage(data:data)!
                
                let arImg = ARReferenceImage(image.cgImage!, orientation: CGImagePropertyOrientation.up, physicalWidth: 0.3)
                arImg.name = url
                arImages?.insert(arImg)
            }
          
//            print("Download Finished")
            config.detectionImages = arImages
            start()
        }
    }
    
    func addAnchor(x: Float, y: Float, z: Float, name: String) {
        let pos = cameraTransform.translation
        
        let anchor = ARWorldAnchor(name: name, column3:[pos.x + x, pos.y + y, pos.z + z, 1])
        session.add(anchor: anchor)
        
//        print(anchor.transform)
    }
    
    func placeModel(id: String, origin: Int) {
        if let anchor = scene.anchors.first(where: {String($0.id) == id}) {
            anchor.children.removeAll()
            
            if origin >= 0 && origin < modelEntities.count, let model = modelEntities[origin] {
                anchor.addChild(model)
                
                var pos = cameraTransform.translation
                pos.y = anchor.position.y
                anchor.look(at: pos, from: anchor.position, upVector: [0, 1, 0], relativeTo: nil)
            }
        }
    }
    
    /// Set the plane orientation to detect
    func changeDetectionType(type: String) {
        NSLog("Thien " + type)
        switch type {
            case "image":
                coachingOverlay.setActive(true, animated: true)
                config.detectionImages = arImages
                config.planeDetection = []
                break
            case "plane":
                coachingOverlay.setActive(true, animated: true)
                config.planeDetection = [.horizontal, .vertical]
                config.detectionImages = nil
                break
            default:
                // none
                coachingOverlay.setActive(false, animated: true)
                config.planeDetection = []
                config.detectionImages = nil
        }
        // and update runtime config
        self.start()
    }
    
    
    func takeSnapshot(requestId: Int) {
        snapshot(saveToHDR: false) { (image) in
            let imageString = image?.jpegData(compressionQuality: 1)?.base64EncodedString() ?? ""
            if (self.onDataReturnedHandler != nil) {
                self.onDataReturnedHandler([
                    "requestId": requestId,
                    "result": imageString,
                    "error": ""
                ])
            } else if(self.onErrorHandler != nil) {
                self.onErrorHandler(["message": "No data handler found to return the snapshot"])
            }
        }
    }
    
    /// Change the model to render
//    func changeEntity(modelEntities: Array<Entity?>) {
//        if (isModelVisible) {
//            for grid in self.grids {
//                if (grid.isShowingModel) {
//                    grid.replaceModel(model: modelEntities[0]!)
//                }
//            }
//        }
//        self.modelEntities.append(contentsOf: modelEntities)
//        tryInstantPlacement(id: 0)
//    }
    
    /// Enable/Disable coaching view
    func setInstructionsVisibility(isVisible: Bool) {
        guard (self.subviews.firstIndex(of: coachingOverlay) != nil) else {
            // no coaching view present
            if (isVisible) {
                coachingOverlay.activatesAutomatically = true
                coachingOverlay.setActive(true, animated: true)
            }
            return
        }
        
        // coaching is present
        if (!isVisible) {
            coachingOverlay.activatesAutomatically = false
            coachingOverlay.setActive(false, animated: true)
        }
    }
    
    /// Enable/Disable environment occlusion
    func setDepthManagement(isEnabled: Bool) {
        if #available(iOS 13.4, *) {
            if(isEnabled) {
                environment.sceneUnderstanding.options.insert(.occlusion)
            } else {
                environment.sceneUnderstanding.options.remove(.occlusion)
            }
        }
    }
    
    /// Enable/Disable light estimation
    func setLightEstimationEnabled(isEnabled: Bool) {
        config.isLightEstimationEnabled = isEnabled
        start()
    }
    
    
    /// Enable/Disable instant placement mode
    func setInstantPlacementEnabled(isEnabled: Bool) {
        self.isInstantPlacementEnabled = isEnabled
//        tryInstantPlacement(id: 0)
    }
    
    
    /// Try to automatically add the model on the first anchor found
//    func tryInstantPlacement(id: Int) {
//        if (isInstantPlacementEnabled && !isModelVisible && !grids.isEmpty && !self.modelEntities.isEmpty) {
//            // place it on first anchor
//            guard let grid: Grid = grids.first else {
//                return
//            }
//            self.showModel(grid: grid, model: self.modelEntities[id]!)
//
//            var pos = cameraTransform.translation
//            pos.y = grid.position.y
//            grid.look(at: pos, from: grid.position, upVector: [0, 1, 0], relativeTo: nil)
//        }
//    }
    
    /// Register all gesture handlers
    func setGestures() {
        // reset all gestures
        for gestureRecognizer in gestureRecognizers! {
            guard let index = gestureRecognizers?.firstIndex(of: gestureRecognizer) else {
                return
            }
            if (gestureRecognizer.name != "gridTap") {
                gestureRecognizers?.remove(at: index)
            }
        }
        // install new gestures
//        for grid in grids {
//            if (grid.isShowingModel) {
//                installGestures(.init(arrayLiteral: self.allowedGestures), for: grid)
//            }
//        }
    }
    
    /// Enable/Disabled user gesture on model: rotation
    func setAllowRotate(isEnabled: Bool) {
        if (isEnabled) {
            self.allowedGestures.insert(.rotation)
        } else {
            self.allowedGestures.remove(.rotation)
        }
        setGestures()
    }
    
    /// Enable/Disable user gesture on model: scale
    func setAllowScale(isEnabled: Bool) {
        if (isEnabled) {
            self.allowedGestures.insert(.scale)
        } else {
            self.allowedGestures.remove(.scale)
        }
        setGestures()
    }
    
    
    /// Enable/Disable user gesture on model: translation
    func setAllowTranslate(isEnabled: Bool) {
        if (isEnabled) {
            self.allowedGestures.insert(.translation)
        } else {
            self.allowedGestures.remove(.translation)
        }
        setGestures()
    }
    
    
    /// Converts degrees to radians
    func deg2rad(_ number: Int) -> Float {
        return Float(number) * .pi / 180
    }
    
    /// Rotate the model
//    func rotateModel(pitch: Int, yaw: Int, roll: Int) -> Void {
//        guard isModelVisible else { return }
//        for plane in self.grids {
//            if (plane.isShowingModel) {
//                let transform = Transform(pitch: deg2rad(pitch), yaw: deg2rad(yaw), roll: deg2rad(roll))
//                let currentMatrix = plane.transform.matrix
//                let calculated = simd_mul(currentMatrix, transform.matrix)
//                plane.move(to: calculated, relativeTo: nil, duration: 1)
//            }
//        }
//    }
    
    // Set our events handlers
    /// Set on started event
    func setOnStartedHandler(handler: @escaping RCTDirectEventBlock) {
        onStartedHandler = handler
    }
    
    /// Set on error event
    func setOnErrorHandler(handler: @escaping RCTDirectEventBlock) {
        onErrorHandler = handler
    }
    
    /// Set on data returned handler (used to resolve promises on JS side)
    func setOnDataReturnedHandler(handler: @escaping RCTDirectEventBlock) {
        onDataReturnedHandler = handler
    }
    
    /// Set on ended event
    func setOnEndedHandler(handler: @escaping RCTDirectEventBlock) {
        onEndedHandler = handler
    }
    
    /// Set on model placed handler
    func setOnAnchorAddedHandler(handler: @escaping RCTDirectEventBlock) {
        onAnchorAdded = handler
    }
    
    /// Set on model removed handler
    func setOnEntityTapHandler(handler: @escaping RCTDirectEventBlock) {
        onEntityTap = handler
    }
    
    /// Add a grid when an new anchor is found
    func session(_ session: ARSession, didAdd anchors: [ARAnchor]) {
        for anchor in anchors {
            if anchor is ARImageAnchor {
//                NSLog("Thien " + (imageAnchor.referenceImage.name ?? " "))
                let grid = AnchorEntity(anchor: anchor)
                grid.transform.matrix = anchor.transform
                self.scene.addAnchor(grid)
                onAnchorAdded(["detected":(anchor as! ARImageAnchor).referenceImage.name ?? "",
                               "type":"image", "id":String(grid.id)])
            }
            
            if anchor is ARPlaneAnchor {
                let grid = AnchorEntity(anchor: anchor)
                grid.transform.matrix = anchor.transform
//                self.grids.append(grid)
                // try instant placement on first anchor placed
//                tryInstantPlacement(id: 0)
                self.scene.addAnchor(grid)
                onAnchorAdded(["detected":"", "type":"plane", "id":String(grid.id)])
            }
            
            if anchor is ARWorldAnchor {
                let grid = AnchorEntity(anchor: anchor)
                grid.transform.matrix = anchor.transform
//                print(anchor.transform)
                self.scene.addAnchor(grid)
                onAnchorAdded(["detected":anchor.name ?? "", "type":"world", "id":String(grid.id)])
            }
        }
    }
        
    /// Update grid position when its anchor moves
    func session(_ session: ARSession, didUpdate anchors: [ARAnchor]) {
//        for anchor in anchors {
//            guard let planeAnchor = anchor as? ARPlaneAnchor else { return }
//            let updatedGrids: [Grid]? = grids.filter { grd in
//                grd.planeAnchor.identifier == planeAnchor.identifier }
//            for grid in updatedGrids ?? [] {
//                if (!grid.isShowingModel && grid.isEnabled) {
//                    grid.transform.matrix = planeAnchor.transform
//                }
//                grid.didUpdate(anchor: planeAnchor)
//            }
//        }
    }
    
    /// Remove a grid from scene when its anchor is removed
    func session(_ session: ARSession, didRemove anchors: [ARAnchor]) {
//        for anchor in anchors {
//            guard let planeAnchor = anchor as? ARPlaneAnchor else { return }
//            let deletedGrids: [Grid]? = grids.filter { grd in
//                grd.planeAnchor.identifier == planeAnchor.identifier }
//            var modelRemoved = false
//            for grid in deletedGrids ?? [] {
//                if (grid.isShowingModel) {
//                    self.isModelVisible = false
//                    modelRemoved = true
//                }
//                self.scene.anchors.remove(grid)
//                self.grids.remove(at: self.grids.firstIndex(of: grid)!)
//            }
//            if (modelRemoved) {
//                self.reset()
//            }
//        }
    }
    
    /// Replace a Grid with the requested model when tapped
//    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
//        guard let touchInView = sender?.location(in: self) else {
//            return
//        }
//
//        guard let modelTapped = self.entity(at: touchInView) else {
//              // nothing hit
//              return
//        }
//
//        guard let gridTapped = modelTapped.parent as? Grid else {
//            return
//        }
//
//        self.showModel(grid: gridTapped, model: self.modelEntities[0]!)
//    }
}
